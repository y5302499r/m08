![foto](/images/Captura_de_pantalla_de_2019-09-19_16-18-18.png)

## Achraf Belgasmi

*Edad : 20 años*

*Direccion : Via laietana 54 P01*

*y5302499r@iespoblenou.org*

## FORMACIÓN
                   
| Año| Curso |
| ---------- | ---------- |
| actualmente  | Grado medio microinformatica y redes (Institut poblenou ecaib, Barcelona)  |
| 2016 - 2017 | PFI Informática (Fundación Adsis, Barcelona)  |
| 2015 - 2016 | 1º de Bachillerato (Instituto técnico Lemon, Marruecos)  |
| 2012 - 2015 | Educación Secundaria Obligatoria (Colegio El Qods, Marruecos)  |

## COMPETENCIAS

**CMS Wordpress:** *Diseño,creación y mantenimiento de páginas web*

**Woocommerce: Diseño,** *creación y mantenimiento de e-commerce*

**HTML5:** *Diseño básico de maquetación de páginas web*

**Photoshop:** *Edición de imágenes*

**Redes y sistemas:** *Conexiones básicas con cableado*

**Ofimática:** *Dominio medio de programas ofimáticas*

**Servicios Cloud:** *Manejo de la suite de Google Drive*

**Hardware:** *Dominio y conocimiento de los componentes de un PC*

**Camtasia Studio:** *Edición y creación de contenidos multimedia*

