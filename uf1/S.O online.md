In this new practice we will explore different Online **Operating Systems** that have similar functionalities to those that can offer desktop operating systems.

It is important:
- Explore on the Internet any point that we do not know how it works.
- For each point, write the report that the teacher will evaluate.
- Bold points are required to be in the report.

Choose one of the following Services and document it properly in the report. Write the specific features from one of this Online Operating System:
- [**OODesk.**] (http://www.oodesk.com/) Operating system Online. Make an account and test all the features you have on the web. Horbito
- [**SilveOS.**] (http://www.silveos.com/) Operating system only enabled for Microsoft Explorer. Graphically very similar to Windows versions
- [**Chrome OS.**] (http://getchrome.eu/) Install and configure Chrome OS on a virtual machine and evaluate the possibilities it has.
- [**Dissident.ai.**] (https://www.dissident.ai/) It is an environment that allows the management of different social networks through an interface and menu.
- [**On Works**] (https://www.onworks.net/os-distributions/debian-based/free-zorinos-online) Simulate different Linux operating systems.
- 

**Extra Point**
- [**AWS Amazon.**] (https://aws.amazon.com/en/) Register within AWS Amazon and analyze what possibilities it offers at the infrastructure and services level.
- [**OwnCloud.**] (https://owncloud.org/) Install using a docker version of this OS and see what features it has. [Owncloud docker] (https://doc.owncloud.org/server/latest/admin_manual/installation/docker/)