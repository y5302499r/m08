# P3. Manage group tools

### Manage groups

For 4 hours we will work in P3. We explore different tools for the management of groups and their organization in social networks. 

We will deal with different applications. For each of them, it must be filled a little report.

**It is important:**
> + Surf on the Internet any question we do not know how it works. <br>
> + For each one, write the report. <br>
> + Points in **red** are required to be in the report.

Comparison between different group organizers / tasks

1. Make an account on **Asana, Trello and Redbooth** and test ALL of the features you see there. Then you perform a small report with advantages and disadvantages that you find in each web application.



2. **Doodle** Analyze this web application and simulate a meeting of your organization with some possible suppliers or customers. CAPTURES and explanation in the report.


**Aqui he creado un doodle para que los trabajadores pueden elijen que dia hacen el reunión**

![Imagen](images/Captura_de_pantalla_de_2019-10-07_09-11-48.png)



3. **Gantt Project.** In environments where you have to plan a project it is very useful and very common to use a tool that helps you see in a diagram how the tasks are organized, who is responsible and when they must be carried out. Make a Gantt diagram with this tool that will serve you for your organization. You can make a Gantt diagram of your synthesis project, for example (from September to June) with the forecast of tasks, times and resources that you plan today to allocate.

![imagen](images/Captura_de_pantalla_de_2019-10-07_09-50-35.png)

4. **MediaWiki.** In every organization a wiki is required where all the workers know and can modify the common points. Install and configure MediaWiki and make a page with the basic rules of your organization. CAPTURES and explanation of what is being done, guide you through the help links that you will find in Moodle.

Everytime you need, you can include any significant web content such as images or videos in your report.

### Social networks tools

We will explore different tools for managing our **social networks.**

Focus on the following applications, social network analyzers, and describe in the report how it can affect your organization.

Choose two applications and answer:
+ Analyze the functionalities that each tool allows and write it down in the report.

    Application | Functionalities
    --- |---
    Reportes + | estadisticas <br> 
    
+ What social networks can you control?
 Instagram
+ What can you do for each social network?
 puedo controlar nuesta red  sociol desde este app
+ For which reason you think your company will be interested in this application? (For what can be useful for your organization?)
Porque puedes conrolar todo por ejemplo Porque puedes controlar todo por ejemplo si algún te deja de segir te sale allí que te han dejado
Make **CAPTURES** of its operation and a **List** of its main functionalities.

Choose two Applications from the following or the moodle links:

+ Tweetdeck
+ Hootsuite
+ HowSociable
+ Buffer.com



