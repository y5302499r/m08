# Introduction of my APP

 > **__Think about the importance of this practice, it is a 25% (Report & AFIs Work) of your grade, and the exam is 25%__**
 
 <br>

## Calendar

**21/10 & 23/10** Prepare all the features of my application. You have to be an expert of your application and you have to do a slide presentation to introduce it to your colleagues.

**28/10** Introduce my app to my AFI group & receive the assignment task to work

**30/10** Prepare the information about the report & presentation with AFI group

**6/11** APP presentations day

**11/11** One hour exam about all SMIX-AFI presentations

<br><br>

## REPORT

#### What I have to know about my App Before visit AFI Group.

1. **PURPOSE** Which is the purpose of my app. Explain here.

Es un sitio web que nos permite generar un contenido combinado de texto, imágenes y audio

2. **USER & PASS** If it is necessary generate a common user to use with AFI group. (User & Pass)

si quieres crear ou compartir algun presentacion o documeno es necesario crear una cuenta  

3. **FEATURES** Describe the main characteristics about my app. Minimum 8.

Compartir las presentaciones online
Permite crear comunidad. Tanto entre empleados como entre clientes o usuarios.
Permite monitorizar las publicaciones.
Las presentaciones pueden ser públicas o privadas.
Puedes hacer una presentacion con audio 
con la cuenta en linkedin puedes acceder a slideshere
Puedes descargar presentaciones ou guardarlas en tu cuenta 
Con LinkedIn Learning Puedes hacer cursos online 


<br>

    Feature1:Compartir las presentaciones online

    Description1: Sólo hay que subir el archivo elegido y listo, ya tienes un enlace que compartir o que adjuntar donde quieras 

    Images1 (how it works): .....
![Imagen](images/Captura_de_pantalla_de_2019-10-21_09-57-26.png)
<br>

    Feature2: Puedes descargar presentaciones ou guardarlas en tu cuenta 

    Description2: Si quieres descargar ou guardar algun presentacione pore X motivo puedes hacerlo 

    Images2 (how it works): .....
![imagen](images/Captura_de_pantalla_de_2019-10-21_10-26-23.png)
<br>

<br>

    Feature3: Las presentaciones pueden ser públicas o privadas.

    Description3: Cunado publicas una presentacion  tienes que elijir en que modo la quieros 

    Images3 (how it works): 
    
    [imagen](images/Captura_de_pantalla_de_2019-10-21_09-53-24.png)

<br>


<br>

    Feature4: .....

    Description4: .....

    Images4 (how it works): .....

<br>


<br>

    Feature5: .....

    Description5: .....

    Images5 (how it works): .....

<br>


<br>

    Feature5: .....

    Description5: .....

    Images5 (how it works): .....

<br>
<br>

    Feature6: .....

    Description6: .....

    Images6 (how it works): .....

<br>


<br>

    Feature7: .....

    Description7: .....

    Images7 (how it works): .....

<br>


<br>

    Feature8: .....

    Description8: .....

    Images8 (how it works): .....

4. **SITUATIONS** Think about three different situations where it can be applied.

  - Situation1:  
  - Situation2:
  - Situation3:


5. **PRESENTATION** MAKE A GOOD PRESENTATION TO INTRODUCE YOUR APP TO AFI GROUP (Slideshare or Prezzi could be a good option)



  It is necessary to keep in mind the following thinks. Talk about:

    - I present myself and my application.

    - What does this application solve? All applications are made to solve a need.

    - Contextualize the application: It's google, zoho, office ...

    - Functionalities of this application

    - Similar applications, as is the market (benchmarking) pros and cons against competence apps.

    - Simulation of this application

    - Why did I choose this application?

    - If you were a programmer, how would you improve it?


 
  A presentation must be:

    - Using visual elements, little text and **NEVER** read the presentation.

    - Use interaction with the users.

    - Real simulation of the application.

    - Think what I can say in 5 minutes that is fun and at the same time rigorous.




