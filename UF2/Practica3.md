#### 1    Crea un directorio
#### 2    Crear un archivo
#### 3    Hacer un repositorio git en este directorio
#### 4    Pon el archivo creado en el área del escenario
#### 5    Haz el primer commit con mensaje
#### 6    Comprueba el estado de git
#### 7    Verifica los commits históricos
#### 8    Modificar el archivo inicial
#### 9    Comprueba el estado de git
#### 10    Agréguelo al área del escenario
#### 11   Comprueba el estado de git
#### 12    Realizar una nueva confirmación con mensaje
#### 13    Cree dos archivos nuevos y agréguelos al área del escenario con "git add".
#### 14    Hacer una nueva confirmación sin mensaje
#### 15    Luego ponga el mensaje y salga: wq
#### 16    En el primer archivo hacemos una modificación
#### 17    Agregue el archivo al área del escenario
#### 18    Realizamos un cambio en el archivo nuevamente
#### 19    Veamos el estado git
#### 20    Nos comprometemos ¿Qué tenemos en el compromiso?
#### 21    Veamos el estado git nuevamente
#### 21    Agregamos el archivo nuevamente al área del escenario
#### 22    Veamos el estado git nuevamente
#### 23    Creamos un nuevo commit
#### 24    Recuperamos uno de los primeros commits
#### 25    Notamos que tenía el primer archivo en esta versión
#### 26    Volvemos al commit actual
#### 27    y veamos qué tiene ahora el archivo


### Commandos

#### git config --global user.name "Achraf"
#### git config --global user.email "y5302499r@iespoblenou.org"
#### mkdir achraf
#### cd achraf
#### touch hola
#### git init 
#### git add hola 
#### git commit -m "First commit"
#### git status
#### git log
#### echo "Hola lunes" > hola 
#### touch 2
#### touch 3 
#### git add .
#### git status 
#### git commit -m "wq"



