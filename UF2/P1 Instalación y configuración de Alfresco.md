## P1. Installation and configuration of Alfresco

#### Installation

Given the Alfresco documentation, install this file manager on your virtual machine.

**Document** all steps at the time of installation (WITH EXPLANATIONS AND IMAGES).

During the installation (PUT ALL THE STEPS) Investigate what are the different technologies used and which ports each service uses: **Postgresql, java, tomcat, etc.** For example you can make a table with: the name of technology, the purpose, the official website, the port, and troubleshouttings you would like to highlight.

Document everything you do and explain it.


#### Configuration

Once you have documented the entire installation, complete the following section, think you have to support INS POBLENOU.

In order to follow this section look at the video tutorials explaining the different parts to be modified. [VideoTutorials](https://docs.alfresco.com/community/topics/alfresco-video-tutorials.html) MAKE A CAPTURE IN EACH CASE, AND AN EXPLANATION WHEN YOU NEED

1. Customize your **MAIN PAGE** with the necessary COMPONENTS, do it as you think you can go better and show the final result with a capture.

2. **Update the personal information** of your profile, including data, photos and social networking accounts.

3. **Create three sites** that give coverage to the different levels of visibility. In each site you give an explanatory name of what's in it and make a site of each type: public, moderate and private. How will you allocate people to the moderate site? How can a user enter in a site? What profiles are there?

4. **Customize the three sites** with different columns in each site. Put the important information on each site. What are Dashlets? What different dashlets can you include in the site?

5. **Add different components to each site**. Add a calendar, a wiki and some other feature in each sites.

6. Which **profile** do I must have to change the Site profile data?

7. Now remove one of the **tabs** that you have in a Site and show the result before and after.

8. Now it creates within the Document Library three folders, in the first folder creates three different types of files. Save them and also put "tags" so they can be searched for. In the second drag three files into the folder. In the third you upload the file you want.

9. Search the files for the **tags** in the previous point.

10. Then change one of the previous documents with **Google Docs**. Then you go to the Check in Google Docs option and you can save the VERSION of this document and add a comment.

11. Create 5 new users to remember your password.

12. Upload multiple users. How to add users to the platform using a **CSV**, do it and copy the process in captures.

13. Assign them to one of your sites, with different profiles: collaborator, contributor and consumer. What kind of **permissions** does each one have with respect to a file?

14. **Version files**. Modify an existing file and save it as a "minor change". Then reload it and modify it extensively from a user that can modify this file, now save it as a major "change". Finally, go back to the original user, look in the original version of the original document and create a new version by reverting. Do you find it useful? Why?

15. Now create a group where you add the 5 previous users. On the 2nd Site, choose a file from the Document Library and include all possible permissions with this group.

16. In Tasks -> My Tasks you will find where you can create a **Workflow** for your workgroup. Create a task for each member of your group. What is the workflow for?

17. In a folder, it **creates a new rule** that consists in sending and email to all the group members when a new file is created or inserted in a folder.

18. Through the site manager you are able to **modify the permissions** that the sites and their users have. Enter the site manager, change the site that was public and make it "moderate", which was private "moderate" and the one that was "moderate" made public. Then remove one of the two "moderate" sites. In the public site, all user permissions "consumer to collaborator" are changed.

19. Create a **model** where there is a text field indicating **who is responsible** for the document, and indicate which training cycle is in a **list (SMIX, DAM, DAW and ASIX)**. It also wants to be indicated if the document **has been published with a variable (true / false)** and the **date of publication**. Apply this model to a document and show the information.