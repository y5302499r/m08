Cree los tres tipos de fusiones explicados en la clase: 

Create the three types of merges explained in class:

1. Recursive merge: two branches with different commits but with files that do not create a conflict between them.
![image](images/Captura_de_pantalla_de_2019-12-11_12-05-00.png)
2. Fast-forward merge: one branch shares all the commits of the other, plus some commit, serves to match branches.
![image](images/Captura_de_pantalla_de_2019-12-09_10-12-47.png)

3. Conflict: There are two branches with a commit with the same file modified in each branch, you must modify the file and commit a new version to merge.
![image](images/Captura_de_pantalla_de_2019-12-11_11-25-15.png)


##EXERCISE 2
Describe the commit chain, merge, branch ... needed to run this project. Keep in mind the order of execution of commits. Invoke files to make commits.


  *  10  mkdir Exercise2
*    11  cd Exercise2/
*    12  git init 
*    13  touch 1
*    14  git add .
*    15  git commit -m "C1"
*    16  git tree
*    17  touch 2
*    18  git add .
*    19  git commit -m "C2"
*    20  git tree
*    21  git branch server
*    22  git checkout server 
*    23  touch 1.1
*    24  git add .
*    25  git commit -m "C3"
*    26  git tree
*    27  git branch client
*    28  git checkout client 
*    29  touch 1.2
*    30  git add .
*    31  git commit -m "C8"
*    32  git tree
*    33  touch 2.2
*    34  git add .
*    35  git commit -m "C9"
*    36  git tree
*    37  git checkout server 
*    38  touch 2.5
*    39  git add
*    40  git add .
*    41  git commit -m "C4"
*    42  git tree
*    43  touch 2.6 
*    44  git add .
*    45  git commit -m "C10"
*    46  git tree
*    47  git checkout master 
*    48  touch 4
*    49  git add .
*    50  git commit -m "C5"
*    51  git tree
*    52  touch 7
*    53  git add .
*    54  git commit -m "C6"
*    55  git tree
*    56  git checkout client 
*    57  git tree
*    58  git branch 
